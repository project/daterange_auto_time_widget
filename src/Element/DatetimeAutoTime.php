<?php

namespace Drupal\daterange_auto_time_widget\Element;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\Element\Datetime;


/**
 * Provides a datetime element where the user can leave it empty and the field gets an auto time defined in widget form settings.
 *
 * @FormElement("datetime_auto_time")
 */
class DatetimeAutoTime extends Datetime {


  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $time_default = '';

    return [
      '#date_time_auto_time' => $time_default
    ] + parent::getInfo();

  }


  /**
   * {@inheritdoc}
   */
  public static function validateDatetime(&$element, FormStateInterface $form_state, &$complete_form) {

    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);

    if ($input_exists) {

      $title = !empty($element['#title']) ? $element['#title'] : '';
      $date_format = $element['#date_date_element'] != 'none' ? static::getHtml5DateFormat($element) : '';
      $time_format = $element['#date_time_element'] != 'none' ? static::getHtml5TimeFormat($element) : '';
      $time_auto_default = $element['#date_time_auto_time'];
      $format = trim($date_format . ' ' . $time_format);

      // If time_auto is set && the date object could not be created but there's date input, try to set a new date object with the auto time
      if ($time_auto_default != '' && $element['#date_time_element'] = 'time'
          && !isset($input['object']) && isset($input['date']) && empty(!isset($input['time']))) {
        $input['time'] = $time_auto_default;
        $datetime_default = new DrupalDateTime($input['date'] . ' ' . $time_auto_default);

        if ($datetime_default instanceof DrupalDateTime && !$datetime_default->hasErrors()) {
          $form_state->setValueForElement($element, $datetime_default);
          $input['object'] = $datetime_default;
        }
      }
      // If there's empty input and the field is not required, set it to empty.
      if (empty($input['date']) && empty($input['time']) && !$element['#required']) {
        $form_state->setValueForElement($element, NULL);
      }
      // If there's empty input and the field is required, set an error. A
      // reminder of the required format in the message provides a good UX.
      elseif (empty($input['date']) && empty($input['time']) && $element['#required']) {
        $form_state->setError($element, t('The %field date is required. Please enter a date in the format %format.', ['%field' => $title, '%format' => static::formatExample($format)]));
      }
      else {
        // If the date is valid, set it.
        $date = $input['object'];
        if ($date instanceof DrupalDateTime && !$date->hasErrors()) {
          $form_state->setValueForElement($element, $date);
        }
        // If the date is invalid, set an error. A reminder of the required
        // format in the message provides a good UX.
        else {
          $form_state->setError($element, t('The %field date is invalid. Please enter a date in the format %format.', ['%field' => $title, '%format' => static::formatExample($format)]));
        }
      }
    }
  }

}
