<?php

namespace Drupal\daterange_auto_time_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;


/**
 * Plugin implementation for the field DatetimeRange.
 *
 * @FieldWidget(
 *   id = "daterange_auto_time",
 *   label = @Translation("Datetime range Auto time"),
 *   field_types = {
 *     "daterange"
 *   },
 * )
 */
class DateRangeAutoTimeWidget extends DateRangeDefaultWidget {


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if ($this->getFieldSetting('datetime_type') == DateRangeItem::DATETIME_TYPE_DATETIME) {

      $times_formatted = $this->getDefaultTimesFormatted();
      $element['value']['#type'] = 'datetime_auto_time';
      $element['value']['#date_time_auto_time'] = $times_formatted['start'];
      $element['end_value']['#type'] = 'datetime_auto_time';
      $element['end_value']['#date_time_auto_time'] = $times_formatted['end'];

      $description['start'] = t('Empty start time will take by default %time', ['%time' => $times_formatted['start']]);
      $description['end'] = t('The end time will take by default %time', ['%time' => $times_formatted['end']]);
      $element['bottom_description'] = ['#markup' => '<p class="dr-at-description">' . $description['start'] . '. ' . $description['end'] . '</p>'];

      $element['#disable_inline_form_errors'] = TRUE;
      $element['#attached']['library'][] = 'daterange_auto_time_widget/daterange_auto_time';
    }

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'start' => [
        'start_hour' => '00',
        'start_minutes' => '00',
        'start_seconds' => '00',
      ],
      'end' => [
        'end_hour' => '23',
        'end_minutes' => '59',
        'end_seconds' => '59'
      ]
    ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $settings = $this->getSettings();

    if ($this->getFieldSetting('datetime_type') == DateRangeItem::DATETIME_TYPE_DATETIME) {
      for ($i = 1; $i <= 2; $i++) {
        $date = $i == 1 ? 'start' : 'end';

        $element[$date] = [
          '#type' => 'fieldset',
          '#title' => t('Default @date time value', ['@date' => $date]),
          '#attributes' => ['class' => ['container-inline']]
        ];

        $element[$date][$date . '_hour'] = [
          '#type' => 'select',
          '#title' => t('Hour'),
          '#required' => TRUE,
          '#options' => $this->getOptions(0, 23),
          '#default_value' => $settings[$date][$date . '_hour']
        ];

        $element[$date][$date . '_minutes'] = [
          '#type' => 'select',
          '#title' => t('Minutes'),
          '#required' => TRUE,
          '#options' => $this->getOptions(),
          '#default_value' => $settings[$date][$date . '_minutes']
        ];

        $element[$date][$date . '_seconds'] = [
          '#type' => 'select',
          '#title' => t('Seconds'),
          '#required' => TRUE,
          '#options' => $this->getOptions(),
          '#default_value' => $settings[$date][$date . '_seconds']
        ];
      }
    }

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getFieldSetting('datetime_type') == DateRangeItem::DATETIME_TYPE_DATETIME) {
      $formatted_times = $this->getDefaultTimesFormatted();

      for ($i = 1; $i <= 2; $i++) {
        $date = $i == 1 ? 'start' : 'end';
        $summary[] = t('Default @date time: %time', ['@date' => $date, '%time' => $formatted_times[$date]]);
      }
    }

    return $summary + parent::settingsSummary();
  }


  /**
   * Return an array with start and end default times with format H:i:S
   *
   * @return array
   */
  private function getDefaultTimesFormatted() {
    $settings = $this->getSettings();

    $times['start'] = $settings['start']['start_hour'] . ':' . $settings['start']['start_minutes'] . ':' . $settings['start']['start_seconds'];
    $times['end'] = $settings['end']['end_hour'] . ':' . $settings['end']['end_minutes'] . ':' . $settings['end']['end_seconds'];

    return $times;
  }


  /**
   * Return an array of 2 digit numbers between a range
   *
   * @param int $min
   * @param int $max
   * @return array
   */
  private function getOptions($min = 0, $max = 59) {
    $options = [];
    $values = range($min, $max);

    foreach($values as $key => $value) {
      $op = str_pad($value, 2, '0', STR_PAD_LEFT);
      $options[$op] = $op;
    }

    return $options;
  }

}
